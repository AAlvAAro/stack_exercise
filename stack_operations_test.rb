require 'minitest/autorun'
require_relative './stack_operations'

describe Stack do
  before do
    @stack = Stack.new
  end

  describe '#add_query' do
    it 'adds a query to the queries stack' do
      @stack.add_query(['1', '90'])
      @stack.add_query(['2'])
      @stack.add_query(['3'])

      assert_equal [['1', '90'], ['2'], ['3']], @stack.queries
    end
  end

  describe '#process_query' do
    describe 'when the query is 1' do
      it 'adds the second element of the input to the sequence' do
        @stack.process_query(['1', '90'])

        assert_equal [90], @stack.sequence
      end
    end

    describe 'when the query is 2' do
      it 'removes the last element of the sequence' do
        @stack.sequence = [1, 2, 3, 4]
        @stack.process_query(['2'])

        assert_equal [1, 2, 3], @stack.sequence
      end
    end

    describe 'when the query is 3' do
      it 'prints the maximum value of the sequence' do
        @stack.sequence = [1, 1000, 2, 100, 3]
        assert_output(/1000/) { @stack.process_query(['3']) }
      end
    end
  end

  describe '#perform_operations' do
    it 'runs every query in the queries array' do
      @stack.queries = [['1', '100']]

      assert_send [@stack, :process_query, ['1', '10']]
    end
  end
end
