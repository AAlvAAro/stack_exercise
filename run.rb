require_relative './stack_operations'

stack = Stack.new

# Run console app
puts 'Enter the number of queries to run: '
n = gets.chomp.to_i

puts "Enter one query than hit ENTER. You'll have to write #{n} queries"

n.times do
  query = gets.chomp.split(' ')

  stack.add_query(query)
end

# Process stack operations
puts "\nResult: "
stack.perform_operations
