## How to run the project

1. Run the entrypoint file
   `ruby run.rb`
2. Follow the console instructions to add the number of queries and add 1 query per line
3. The output will be printed to the console

Tests can be run with
   `ruby stack_operations_test.rb`
