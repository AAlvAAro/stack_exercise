class Stack
  attr_accessor :sequence, :queries

  def initialize
    @sequence = []
    @queries = []
  end

  def add_query(query)
    @queries <<  query
  end

  def process_query(query)
    operation = query.first.to_i

    case operation
    when 1
      @sequence << query.last.to_i
    when 2
      @sequence.pop
    when 3
      puts @sequence.max
    end
  end

  def perform_operations
    @queries.each do |query|
      process_query(query)
    end
  end
end
